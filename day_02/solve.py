#!/usr/bin/env python3

def part_one(data):
    depth, horizontal = 0, 0

    while data:
        d, n = data[0].split()
        n = int(n)
        if d == "forward":
            horizontal += n
        elif d == "up":
            depth -= n
        elif d == "down":
            depth += n
        data = data[1:]
    
    return depth * horizontal

def part_two(data):
    depth, horizontal, aim = 0, 0, 0

    while data:
        d, n = data[0].split()
        n = int(n)
        if d == "forward":
            horizontal += n
            depth += aim * n
        elif d == "up":
            aim -= n
        elif d == "down":
            aim += n
        data = data[1:]
    
    return depth * horizontal


if __name__ == '__main__':
    with open("test.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    with open("input.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    print(f"P1: {part_one(data)}")
    print(f"P2: {part_two(data)}")
    