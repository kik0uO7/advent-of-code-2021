#!/usr/bin/env python3

import sys

def get_answer(grid):
    return len([ _ for _ in grid.values() if _ > 1 ])


def part_one(data):
    grid = {}
    while data:
        x1, y1, x2, y2 = data[0].replace(" -> ", ",").split(",")
        if x1 == x2 or y1 == y2:
            p1 = complex(int(x1),int(y1))
            p2 = complex(int(x2),int(y2))
            p3 = p2 - p1
            re, im = int(p3.real), int(p3.imag)
            i, j = 0, 0

            while i != re or j != im:
                p = complex(p1.real + i, p1.imag + j)
                if not p in grid.keys():
                    grid[p] = 0
                # print(re, im, p)
                grid[p] += 1
                if re == 0:
                    j += 1 if p1.imag < p2.imag else -1
                elif im == 0:
                    i += 1 if p1.real < p2.real else -1
            p = complex(p1.real + i, p1.imag + j)
            if not p in grid.keys():
                grid[p] = 0
            grid[p] += 1
        data = data[1:]
    return get_answer(grid)

def part_two(data):
    grid = {}
    while data:
        x1, y1, x2, y2 = data[0].replace(" -> ", ",").split(",")
        if x1 == x2 or y1 == y2 or [x1,y1].sort() == [x2,y2].sort():
            p1 = complex(int(x1),int(y1))
            p2 = complex(int(x2),int(y2))
            p3 = p2 - p1
            re, im = int(p3.real), int(p3.imag)
            i, j = 0, 0

            while i != re or j != im:
                p = complex(p1.real + i, p1.imag + j)
                if not p in grid.keys():
                    grid[p] = 0
                # print(re, im, p)
                grid[p] += 1
                if re == 0:
                    j += 1 if p1.imag < p2.imag else -1
                elif im == 0:
                    i += 1 if p1.real < p2.real else -1
                else:
                    j += 1 if p1.imag < p2.imag else -1
                    i += 1 if p1.real < p2.real else -1

            p = complex(p1.real + i, p1.imag + j)
            if not p in grid.keys():
                grid[p] = 0
            grid[p] += 1
        data = data[1:]
    return get_answer(grid)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f"usage: {sys.argv[0]} [SAMPLE_FILE]")
        sys.exit(-1)

    with open(sys.argv[1], "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    print(f"P1: {part_one(data)}")
    print(f"P2: {part_two(data)}")
    