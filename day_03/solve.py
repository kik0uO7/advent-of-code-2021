def part_one(data):
    maxrow = len(data)
    count = [0] * len(data[0])
    found = [0] * len(data[0])

    for row in data:
        for idx, col in enumerate(row):
            if found[idx] == 1:
                continue
            if count[idx] >= maxrow//2:
                found[idx] = 1
                continue

            if col == "1":
                count[idx] += 1

    gamma = (int("".join(map(str,found)),2))
    epsilon = (int("".join(map(lambda x: str(1-x),found)),2))
    return gamma * epsilon


def get_subset(data, col, value):
    subset = []
    while data:
        if data[0][col] == value:
            subset.append(data[0])
        data = data[1:]
    
    return subset

def get_most_common(data, col):
    count = 0
    datalen = len(data)
    while data:
        if count >= datalen/2:
            return 1
        if data[0][col] == "1":
            count +=1
        data = data[1:]
    return 1 if count >= datalen/2 else 0

def part_two(data):
    o2 = 0
    co2 = 0

    original_data = data
    for idx_col in range(len(data[0])):
        most_common = str(get_most_common(data, idx_col))
        data = get_subset(data, idx_col, most_common)
        if len(data) <= 1:
            o2 = int(data[0], 2)
            break

    data = original_data
    for idx_col in range(len(data[0])):
        most_common = str(1 - get_most_common(data, idx_col))
        data = get_subset(data, idx_col, most_common)
        if len(data) <= 1:
            co2 = int(data[0], 2)
            break

    return o2 * co2

if __name__ == '__main__':
    with open("test.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    with open("input.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    print(f"P1: {part_one(data)}")
    print(f"P2: {part_two(data)}")
    