def part_one(data):
    last = None
    cpt = 0

    while data:
        current = int(data[0])
        if last and last < current:
            cpt+=1
        
        last = current
        data = data[1:]

    return cpt


def part_two(data):
    last = None
    cpt = 0

    while data:
        if len(data) > 2:
            current = int(data[0]) + int(data[1]) + int(data[2])
            if last and last < current:
                cpt+=1
            
            last = current
        data = data[1:]

    return cpt



if __name__ == '__main__':
    with open("test.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    with open("input.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    print(f"Part 1: {part_one(data)}")
    print(f"Part 2: {part_two(data)}")
    