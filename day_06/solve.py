import sys

def part_one(data):
    fishes = [ int(_) for _ in data[0].split(",")]
    days = 0
    while days < 80:
        new_fishes = []
        for fish in fishes:
            if fish == 0:
                new_fishes.append(6)
                new_fishes.append(8)
            else:
                new_fishes.append(fish - 1)

        fishes = new_fishes
        days += 1
    return len(fishes)


def get_fish(state, day):
    fishes = [(state, day)]
    days = day
    while days < 256:
        new_fishes = []
        for fish in fishes:
            if fish[1] == 2:
                new_fishes.append(())
                new_fishes.append(8)
            else:
                new_fishes.append(fish - 1)

        fishes = new_fishes
        print(days, len(fishes))
        days += 1

    print(len(fishes))

def part_two(data):
    fishes = [ int(_) for _ in data[0].split(",")]
    group_fishes = {
        0 : 0,
        1 : 0,
        2 : 0,
        3 : 0,
        4 : 0,
        5 : 0,
        6 : 0,
        7 : 0,
        8 : 0,
    }

    for f in fishes:
        group_fishes[f] += 1

    days = 0
    while days < 256:
        tmp = group_fishes[0]
        group_fishes[0] = group_fishes[1]
        group_fishes[1] = group_fishes[2]
        group_fishes[2] = group_fishes[3]
        group_fishes[3] = group_fishes[4]
        group_fishes[4] = group_fishes[5]
        group_fishes[5] = group_fishes[6]
        group_fishes[6] = group_fishes[7]
        group_fishes[7] = group_fishes[8]
        group_fishes[8] = tmp
        group_fishes[6] += tmp

        days += 1
        
    batch = 0
    for fishes in group_fishes.values():
        batch += fishes

    return batch


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f"usage: {sys.argv[0]} [SAMPLE_FILE]")
        sys.exit(-1)

    with open(sys.argv[1], "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    print(f"P1: {part_one(data)}")
    print(f"P2: {part_two(data)}")
    