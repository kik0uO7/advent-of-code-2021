def check_win_row(grid, row):
    check_row = grid[row]
    for elem in check_row:
        if elem[1] == 0:
            return False
    return True

def check_win_col(grid, col):
    check_col = [grid[idx][col] for idx in range(5)]
    for elem in check_col:
        if elem[1] == 0:
            return False
    return True

def get_all_unmarked(grid):
    return [int(grid[i//5][i%5][0]) for i in range(25) if grid[i//5][i%5][1] == 0]

def part_one(data):
    rand_num = data[0]
    lookup_table = {}
    grids = []

    for idx, row in enumerate(data[1:]):
        grid_num = idx // 5
        if idx % 5 == 0:
            grids.append([])
        grids[grid_num].append([ (_,0) for _ in row.split(" ") if _])

    for g_idx, grid in enumerate(grids):
        for r_idx, row in enumerate(grid):
            for c_idx, col in enumerate(row):
                if not col[0] in lookup_table.keys():
                    lookup_table[col[0]] = []
                lookup_table[col[0]].append((g_idx, r_idx, c_idx))

    for sample in  rand_num.split(","):
        for g, r, c in lookup_table[sample]:
            grids[g][r][c] = (grids[g][r][c][0],1)
            win = check_win_row(grids[g], r)
            if win:
                return int(sample) * sum(get_all_unmarked(grids[g]))
            win = check_win_col(grids[g], c)
            if win:
                return int(sample) * sum(get_all_unmarked(grids[g]))

def part_two(data):
    rand_num = data[0]
    lookup_table = {}
    grids = []
    wins = []

    for idx, row in enumerate(data[1:]):
        grid_num = idx // 5
        if idx % 5 == 0:
            grids.append([])
        grids[grid_num].append([ (_,0) for _ in row.split(" ") if _])

    for g_idx, grid in enumerate(grids):
        for r_idx, row in enumerate(grid):
            for c_idx, col in enumerate(row):
                if not col[0] in lookup_table.keys():
                    lookup_table[col[0]] = []
                lookup_table[col[0]].append((g_idx, r_idx, c_idx))

    grids_win = [0] * len(grids)

    for sample in  rand_num.split(","):
        for g, r, c in lookup_table[sample]:
            if grids_win[g] == 1:
                continue

            grids[g][r][c] = (grids[g][r][c][0],1)
            win = check_win_row(grids[g], r)
            if win:
                grids_win[g] = 1
                wins.append(int(sample) * sum(get_all_unmarked(grids[g])))
            win = check_win_col(grids[g], c)
            if win:
                grids_win[g] = 1
                wins.append(int(sample) * sum(get_all_unmarked(grids[g])))
    
    return wins

if __name__ == '__main__':
    with open("test.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]

    with open("input.txt", "r") as f:
        data = [ _ for _ in f.read().split("\n") if _ ]


    print(f"P1: {part_one(data)}")
    print(f"P2: {part_two(data)[-1]}")
    